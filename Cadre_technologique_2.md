# Sujet 2 : Le Web est-il devenu trop compliqué ?

### Problématique 

Stéphane Bortzmeyer, dans son article *[Le Web est-il devenu trop compliqué ?](https://framablog.org/2020/12/30/le-web-est-il-devenu-trop-complique/),* se demande si le Web est devenu trop compliqué notamment en raison des technologies sous-jacentes. Il souhaite faire partagé ses idées auprès de tous car il affirme que ce problème n'est pas réservé aux développeur ou développeuses du Web mais bien à tous les usagers du Web.

Au cours de son article, Stéphane Bortzmeyer évoque le projet Gemini, qui a pour ambition de créer (une alternative au Web, le plus simple et basique possible sans toutes les technologies sous-jacentes. Nous pouvons nous demander pourquoi le Web a-t-il besoin de revenir à une entité simple et comment ce projet pourra-t-il émerger.

Dans son article il soutient différentes idées que nous allons tenter de justifier au cours cet article. Dans un premier temps nous verrons l'histoire et le développement du Web concernant ses fonctionnalités puis nous verrons le rôle de l'utilisateur sur le Web. Nous verrons dans un dernier temps les procédés possibles pour le projet Gemini.

### L'histoire et le développement du Web

Tim Berners, en 1990, est à l'origine de l'Internet et du Web que nous connaissons aujourd'hui. Le Web repose sur 3 piliers fondamentaux qui sont :

* Le langage de formatage HTML (HyperText Markup Language)
* L'URI (actuellement appelé URL) qui est une adresse unique et qui est utilisé pour chaque ressource présente sur le Web
* Le protocole HTTP (HyperText Transfer Protocol) qui permet de faire le lien entre des ressources présentes sur le Web.

En 1993, le Web est rendu public et avait pour but de permettre la communication entre des personnes vivant à des endroits différents sur la Terre de manière simple et rapide mais aussi de découvrir de nouvelles avancées qu'elles soient technologiques, culturelles. Le Web se devait d'être universel qui repose sur le langage HTML, décentraliser, neutre et transparent.

Au cours des années le Web s'est complexifié permettant de personnaliser ses pages notamment grâce aux fichiers CSS et Javascript mais aussi d'apporter plus de rapidité à ses utilisateurs. Aujourd'hui, Google qui est le moteur de recherche le plus utilisé au monde avec 65% de part de marché dans le monde [[1]](https://www.blogdumoderateur.com/chiffres-google/), précise le temps de recherche pour trouver les résultats de la requête de son utilisateur. Par exemple la recherche *temps pour une recherche google* nous indique :

> Environ 848 000 000 résultats (0,75 secondes) 

Le Web devient de plus en rapide et comme l'énonce Stéphane Bortzmeyer dans son article, le développement de la 5G et de la fibre est en faveur de son expansion. Cependant, comme il le précise, ce n'est plus le temps de recherche lié à la connexion qui pose des problèmes mais bien tous les flux de données derrière la requête de recherche et l'affichage des pages Web :

> Passer à la fibre ou à la 5G ne se traduira pas forcément par un gain de temps, puisque ce sont souvent les calculs nécessaires à l’affichage qui ralentissent la navigation

Les étapes pour afficher une recherche sur Google nécessite des millions de données. En effet il existe trois étapes pour obtenir le résultat d'une recherche Google [[2]](https://developers.google.com/search/docs/beginner/how-search-works?hl=fr) :

1. Exploration : Il faut identifier quelles pages se trouvent sur le Web car il n'y a pas d'identité centrale. 
2. indexation : Google doit analyser la page pour déterminer le contenu et faire correspondre ou non avec la le recherche de l'utilisateur.
3. Diffusion et Classement : De nombreux paramètres sont pris en compte pour que le résultat de la requête de l'utilisateur soit la plus approprié comme la zone géographique, la langue, ect. Les résultats sont par la suite classé selon leur pertinence.

Ainsi toutes ces requêtes nécessitent un certain puis l'affichage et est géré par un algorithme. De plus il faut prendre en compte l'affichage de la page qui est une autre source de temps non négligeable aujourd’hui avec toutes les propriétés CSS, Javascript.

Ainsi comme le précise Stéphane Bortzmeyer, les pages Web ne sont plus de simples pages Web avec un langage HTML derrière mais elles sont aujourd'hui complétées par : 

> La « feuille de style », rédigée dans le langage CSS, qui va indiquer comment présenter la page, du JavaScript, un langage de programmation qui va être exécuté pour faire varier le contenu de la page, des vidéos, et d’autres choses qui souvent distraient du contenu lui-même.

Finalement le Web selon les mots de Stéphane Bortzmeyer et selon l'accroissement du nombre de pages Web, le Web s'alourdit de fonctionnalités qui dépasse le contenu même que l'on souhaite chercher à la base.

Il faut aussi savoir que l'utilisateur, même celui qui ne fait que naviguer sur le Web, entre autre l'alourdit et c'est ce que nous allons voir dans la deuxième partie.

### Le rôle de l'utilisateur sur le Web

Faire une requête sur le Web n'est plus anodin car comme le précise Stéphane Bortzmeyer

> La complexité du Web cache en effet également cette activité de surveillance, pratiquée aussi bien par les entreprises privées que par les États

En effet, lors de chaque recherche des entreprises privées ou des États collectent des informations pour créer votre *profil utilisateur.* Ce profil est actualisé sans cesse à chaque recherche ou chaque page visitée au travers des cookies. Les cookies sont généralement utilisés par les sites à caractère commercial pour conserver les préférences de l’utilisateur comme les options qu’il a cochées par exemple. De plus ces cookies peuvent servir à des nombreuses entités et c'est pour cela que la CNIL (Commission nationale de l'informatique et des libertés) a décider de créer une loi pour prévenir les utilisateurs sur ce principe et de laisser le choix ou non sur la possibilité d'activer ses cookies. En effet Stéphane Bortzmeyer l'explique dans son article 

> La plupart des pages Web incluent en effet des ressources extérieures (images, vidéos, boutons de partage), pas forcément chargés depuis le site Web que vous visitez et qui ont eux aussi leurs cookies.

Les pages Web parviennent malheureusement a contourné cette loi en activant en avance tous les cookies possibles et en laissant à l'utilisateur, le choix suivant : Confirmer mes préférences / Tout accepter.

Ainsi simplifié le Web en réduisant les pages Web a seulement un fichier HTML et ne pas utiliser de cookies permettrait de rendre le Web plus rapide et plus privé. C'est avec ces données que le projet Gemini a émerger.

### Les procédés possibles pour le projet Gemini

Le projet Gemini est un projet radical car il limite le langage parlé entre le navigateur et le serveur et n'est pas extensible. En effet ce projet se veut revenir aux premières pages Web simples avec la puissance de calcul d'aujourd'hui et d'enlever toutes formes de collecte de données utilisateurs.

Stéphane Bortzmeyer fait remarquer dans son article que

> Gemini est actuellement en cours de développement, de manière très ouverte, notamment sur la liste de diffusion publique du projet

Ainsi on retrouve bien la culture du libre ou chacun peut apporter son aide à ce projet et la notion de transparence est au cœur du projet.

Ce projet pourrait être basé sur les chaines éditoriales car ce procédé de création de contenu multimédia ne fixe pas la mise en forme graphique du contenu a priori à la différence des outils bureautiques. Ce procédé se base sur le WYSIWYG, « What you see is what you get », qui signifie: « Ce que vous voyez est ce que vous obtenez ». Il n'y a ainsi pas de superflus avec des fichiers sous-jacents. Les chaînes éditoriales se veulent tout support et permettent donc de créer n'importe quel type de fichier.

On pourrait alors retrouver toutes les informations qui sont disponibles sur le Web tout en ayant une simplicité et un contrôle qualité sur l'information mise à disposition.

L'entreprise Scenari met à disposition un logiciel permettant de créer une chaine éditoriale multisupport. C'est notamment la façon dont a été créé le site librecours. 

### Conclusion

Le Web devient de plus en plus complexe mais au détriment de l'information que l'on souhaite réellement. De nombreuses voix se lèvent pour prôner la culture du libre et Stéphane Bortzmeyer en est la preuve car le Web doit retrouver les piliers qui faisait et fait de lui l'un des meilleurs outils d'aujourd'hui.

### Licence CC-BY-NC-SA

Cette licence permet de copier, distribuer et communiquer ce fichier dans les mêmes conditions de partage. Il est aussi possible de le transformer mais il est impossible de l'utiliser pour un but commercial. Pour plus amples informations sur la licence, il faut se référer au[ lien suivant](https://creativecommons.org/licenses/by-nc-sa/2.0/fr/).